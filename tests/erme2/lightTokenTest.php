<?php

use erme2\lightToken;
use PHPUnit\Framework\TestCase as PHPUnit_Framework_TestCase;

class lightTokenTest extends PHPUnit_Framework_TestCase
{
    public function test__construct()
    {
        // empty
        try {
            $testBuild = new lightToken();
        } catch (\Exception $exception) {
            $this->assertInstanceOf(\Exception::class, $exception);
            $this->assertEquals(lightToken::ERROR_MESSAGES["empty_saltkey"], $exception->getMessage());
        }

        //default settings
        $saltKey = "thisIsNotASecret";
        $testBuild = new lightToken($saltKey);
        $this->assertInstanceOf(lightToken::class, $testBuild);
        $this->assertEquals(1, $testBuild->expireIn);
        $this->assertEquals(0, $testBuild->hashingMethod);
        $this->assertEquals($saltKey, $testBuild->saltKey);
        $this->assertFalse($testBuild->isDebug);

        // basic test with settings
        $hashingMethod = 0;
        $expireIn = 5;
        $isDebug = true;
        $testBuild = new lightToken($saltKey, $hashingMethod, $expireIn, $isDebug);
        $this->assertInstanceOf(lightToken::class, $testBuild);
        $this->assertEquals($expireIn, $testBuild->expireIn);
        $this->assertEquals($hashingMethod, $testBuild->hashingMethod);
        $this->assertEquals($saltKey, $testBuild->saltKey);
        $this->assertEquals($isDebug, $testBuild->isDebug);
    }

    public function testGenerate()
    {
        // empty time
        $time = 0;
        $saltKey = "thisIsNotASecret";
        $hash = md5("#$time@{$saltKey}#");
        $testBuild = new lightToken($saltKey);
        $this->assertEquals($hash, $testBuild->generate($time));

        // current time
        $time = time();
        $hash = md5("#$time@{$saltKey}#");
        $testBuild = new lightToken($saltKey);
        $this->assertEquals($hash, $testBuild->generate($time));

        // 5 mins ago
        $time = time() - (60 * 5);
        $hash = md5("#$time@{$saltKey}#");
        $testBuild = new lightToken($saltKey);
        $this->assertEquals($hash, $testBuild->generate($time));

        // another saltkey
        // empty time
        $time = 0;
        $saltKey = "thisIsAnotherSaltKey";
        $hash = md5("#$time@{$saltKey}#");
        $testBuild = new lightToken($saltKey);
        $this->assertEquals($hash, $testBuild->generate($time));

        // current time
        $time = time();
        $hash = md5("#$time@{$saltKey}#");
        $testBuild = new lightToken($saltKey);
        $this->assertEquals($hash, $testBuild->generate($time));

        // 5 mins ago
        $time = time() - (60 * 5);
        $hash = md5("#$time@{$saltKey}#");
        $testBuild = new lightToken($saltKey);
        $this->assertEquals($hash, $testBuild->generate($time));

        // properSaltKey
        // empty time
        $time = 0;
        $saltKey = "qP5U#xXpgc2bO%a8ls4xe2MBtCEAwlyjMyHlFN%ak#*@8Zz4^ZhS8aqQ&#!5KHbe";
        $hash = md5("#$time@{$saltKey}#");
        $testBuild = new lightToken($saltKey);
        $this->assertEquals($hash, $testBuild->generate($time));

        // current time
        $time = time();
        $hash = md5("#$time@{$saltKey}#");
        $testBuild = new lightToken($saltKey);
        $this->assertEquals($hash, $testBuild->generate($time));

        // 5 mins ago
        $time = time() - (60 * 5);
        $hash = md5("#$time@{$saltKey}#");
        $testBuild = new lightToken($saltKey);
        $this->assertEquals($hash, $testBuild->generate($time));
    }

    public function testVerify()
    {
        // working
        $time = time();
        $saltKey = "qP5U#xXpgc2bO%a8ls4xe2MBtCEAwlyjMyHlFN%ak#*@8Zz4^ZhS8aqQ&#!5KHbe";
        $testBuild = new lightToken($saltKey);
        $hash = $testBuild->generate($time);
        $this->assertTrue($testBuild->verify($hash, $time));

        // expired
        $expireIn = 1;
        $time = time() - $expireIn - 1;
        $saltKey = "qP5U#xXpgc2bO%a8ls4xe2MBtCEAwlyjMyHlFN%ak#*@8Zz4^ZhS8aqQ&#!5KHbe";
        $testBuild = new lightToken($saltKey, lightToken::HASHING_METHODS['md5'], $expireIn);
        $hash = $testBuild->generate($time);
        $this->assertFalse($testBuild->verify($hash, $time));

        // expired - debug
        $expireIn = 1;
        $time = time() - $expireIn - 1;
        $saltKey = "qP5U#xXpgc2bO%a8ls4xe2MBtCEAwlyjMyHlFN%ak#*@8Zz4^ZhS8aqQ&#!5KHbe";
        $testBuild = new lightToken($saltKey, lightToken::HASHING_METHODS['md5'], $expireIn, true);
        $hash = $testBuild->generate($time);
        try {
            $res = $testBuild->verify($hash, $time);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(\Exception::class, $exception);
            $this->assertEquals(lightToken::ERROR_MESSAGES['expired_token'], $exception->getMessage());
        }

        // wrong saltKey
        $time = time();
        $saltKey = "qP5U#xXpgc2bO%a8ls4xe2MBtCEAwlyjMyHlFN%ak#*@8Zz4^ZhS8aqQ&#!5KHbe";
        $testBuild = new lightToken($saltKey);
        $testCheck = new lightToken("AnotherSalt`KEy");
        $hash = $testCheck->generate($time);
        $this->assertFalse($testBuild->verify($hash, $time));

        // wrong saltKey
        $time = time();
        $saltKey = "qP5U#xXpgc2bO%a8ls4xe2MBtCEAwlyjMyHlFN%ak#*@8Zz4^ZhS8aqQ&#!5KHbe";
        $testBuild = new lightToken($saltKey, lightToken::HASHING_METHODS['md5'], 1, true);
        $testCheck = new lightToken("AnotherSalt`KEy");
        $hash = $testCheck->generate($time);
        try {
            $res = $testBuild->verify($hash, $time);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(\Exception::class, $exception);
            $this->assertEquals(lightToken::ERROR_MESSAGES['wrong_hash'], $exception->getMessage());
        }
    }
}
