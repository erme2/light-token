<?php

namespace erme2;



class lightToken
{
    const HASHING_METHODS = [
        'md5' => 0,
    ];
    const ERROR_MESSAGES = [
        "empty_saltkey" => "The saltKey can not be empty",
        "invalid_hash_method" => "Hash Method not supported",
        "expired_token" => "Expired Token",
        "wrong_hash" => "Wrong Hash"
    ];

    public $expireIn = 1;       // in seconds
    public $hashingMethod = 0;  // check self::HASHING_METHODS
    public $saltKey = "";
    public $isDebug = true;


    /**
     * lightToken constructor.
     *
     * @param string $saltKey
     * @param string|null $hashingMethod
     * @param int|null $expireIn
     * @param bool $debugMode
     *
     * @throws \Exception
     */
    public function __construct(string $saltKey = "", int $hashingMethod = null, int $expireIn = null, bool $isDebug = false)
    {
        if(empty($saltKey)) {
            throw new \Exception(self::ERROR_MESSAGES["empty_saltkey"]);
        }
        $this->saltKey = $saltKey;
        $this->hashingMethod = $hashingMethod ?? $this->hashingMethod;
        $this->expireIn = $expireIn ?? $this->expireIn;
        $this->isDebug = $isDebug;
    }

    /**
     * generates a token
     *
     * @param int $time
     * @param string $saltKey
     * @param int $hashingMethod
     *
     * @return string
     * @throws \Exception
     */
    public function generate(int $time, string $saltKey = null, int $hashingMethod = null) : string
    {
        $saltKey = $saltKey ?? $this->saltKey;

        $hashingMethod = $hashingMethod ?? $this->hashingMethod;

        switch ($hashingMethod) {
            case 0: // md5
                return md5("#$time@{$saltKey}#");
        }
        throw new \Exception(self::ERROR_MESSAGES["invalid_hash_method"]);
    }

    public function verify(
        string $hash,
        int $time,
        string $saltKey = null,
        int $hashingMethod = null,
        int $expireIn = null,
        bool $isDebug = null
    ) : bool
    {
        // updating settings from params
        $saltKey = $saltKey ?? $this->saltKey;
        $hashingMethod = $hashingMethod ?? $this->hashingMethod;
        $expireIn = $expireIn ?? $this->expireIn;
        $isDebug = $isDebug ?? $this->isDebug;

        // checking if the token is expired
        $currentTime = time();
        $timeDiff = abs($currentTime - $time);
        if($timeDiff > $expireIn) {
            if($isDebug) {
                throw new \Exception(self::ERROR_MESSAGES["expired_token"]);
            }
            return false;
        }

        // checking the hash
        $hashCheck = $this->generate($time, $saltKey, $hashingMethod);
        if($hash === $hashCheck) {
            return true;
        }
        if($isDebug) {
            throw new \Exception(self::ERROR_MESSAGES["wrong_hash"]);
        }
        return false;
    }
}